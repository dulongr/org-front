#----------------------------------------
# Calculator in StarTrek logic
# 
# 
#----------------------------------------

# * => 1
# ** => 2 etc...

# ********** => 0
# *********** => +
# ************ => -
# ************* => *
# ************** => /

debug=False

import ply.lex as lex
import ply.yacc as yacc
import sys

def dec_to_star(num):
    ret = ''

    if num >= 10:
        ret += dec_to_star(num//10)
        num = num % 10
    
    ret += num*'*' + ' '

    return ret

def star_to_dec(stars):
    ret = 0
    stars = stars.split()
    for i in stars:
        if i != '':
            ret *= 10
            ret += len(i) % 10
            # Use modulo because 10 stars means 0
    return ret

    
tokens = ('NUMBER', 'PLUS', 'MINUS', 'MULTIPLY', 'DIVIDE', 'MODULO', 'VARIABLE', 'ASSIGN', 'PRINT')


literals = ['*']

def t_NUMBER(t):
    r'(\*{1,10}(\s))+'
    t.value = star_to_dec(t.value)
    if debug: print("Star to dec : " + str(t.value))
    return t


t_VARIABLE = r'(\*){18}\**\s'
t_PRINT = r'\*{17}\s*'
t_ASSIGN = r'\*{16}\s*'
t_MODULO = r'\*{15}\s*'
t_DIVIDE = r'\*{14}\s*'
t_MULTIPLY = r'\*{13}\s*'
t_MINUS = r'\*{12}\s*'
t_PLUS = r'\*{11}\s*'


t_ignore = ' '


def t_error(t):
    print("Illegal character '%s'" % t.value[0])
    t.lexer.skip(1)


lexer = lex.lex()

# lexer.input(code)
# print("**********************************************")

# while True:
#     tok = lexer.token()
#     if not tok:
#         break      # No more input
#     print(tok)

# print("**********************************************")

var = {}

precedence = (
    ('left', 'PLUS', 'MINUS'),
    ('left', 'MULTIPLY', 'DIVIDE'), 
    ('left', 'MODULO'),
    ('left', 'ASSIGN'),
    )


#-----------------------------------

def p_S_assignment(p):
    'S : VARIABLE ASSIGN expression'
    var[p[1]] = p[3]
    p[0] = p[3]


def p_S_print(p):
    'S : PRINT expression'
    print(dec_to_star(p[2]))

def p_S_expression(p):
    'S : expression'
    p[0] = p[1]

#-----------------------------------

def p_expression_plus(p):
    '''expression : expression PLUS expression'''
    p[0] = p[1] + p[3]

    
def p_expression_minus(p):
    '''expression : expression MINUS expression'''
    p[0] = p[1] - p[3]

def p_expression_multiply(p):
    '''expression : expression MULTIPLY expression'''    
    p[0] = p[1] * p[3]


def p_expression_divide(p):
    '''expression : expression DIVIDE expression'''
    p[0] = p[1] // p[3]

def p_expression_modulo(p):
    '''expression : expression MODULO expression'''
    p[0] = p[1] % p[3]
    

def p_expression_var(p):
    'expression : VARIABLE'
    p[0] = var[p[1]]

def p_expression_number(p):
    'expression : NUMBER'
    p[0] = p[1]
    

#-----------------------------------

def p_error(p):
    if p:
        print("Syntax error at '%s'" % p.value)
        print(p)
    else:
        print("Syntax error at EOF")
        
#-----------------------------------
    


def clean(s):
    ret = ''
    for i in s:
        if i in ("*"," "):
            ret += i
    if debug: print("Cleaned: "+ret)
    return ret        


parser = yacc.yacc(debug=True)
if len(sys.argv) != 2:
    print('Usage : python enterprise-v3.py <my-script.startrek>')
else:
    with open(sys.argv[1]) as f:
        lines = f.read().splitlines()
        ret = 0
        for line in lines:
            if debug: print('Line : '+line)
            ret = parser.parse(clean(line+" "))

        if ret:
            print(dec_to_star(ret))

# code = "******************** **************** ****** ************* ********"

# lexer.input(clean(code)+" ")
# print("**********************************************")

# while True:
#     tok = lexer.token()
#     if not tok:
#         break      # No more input
#     print(tok)

# print("**********************************************")
