
#+AUTHOR: Rémi Dulong & Christian Göettel
#+TITLE: Révision Programmation 1
#+LANGUAGE: fr
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../css/style.css" />


* Le concept de cette liste :
Voici une liste d'exercices que vous devriez être
capables de résoudre *les yeux fermés* (ou presque !).
Même lorsque l'on travaille sur papier, il faut que la
logique soit correcte, et que les outils
utilisés qui sont des notions du cours soient maîtrisés.

** _Attention :_
Cette liste n'est qu'une *aide* à la révision, l'examen
portera sur *tout le cours*, et pas uniquement sur cette
liste. En particulier, les exemples simples donnés tout
le long du cours doivent être connus.
Cependant, certains sujets ne seront *pas* à l'examen du tout :

- Histoire de l'Informatique
- Complexité spatiale (seulement la complexité temporelle)
- Intelligence Artificielle
- Ligne de commande


** Comment utiliser ce site ?

Mon conseil est de travailler selon ces étapes :

1. Essayer de résoudre les exercices *sur papier*, sans aide
   extérieure, et sans les slides du cours. Ne pas hésiter à
   passer quand on ne sait pas ou qu'on hésite.
2. Faire la liste des questions pour lesquelles on a pas
   sû répondre, et aller lire les slides du cours qui expliquent
   la notion.
3. Refaire les questions après avoir lu le cours. À cette étape,
   on peut se servir de l'ordi, et tester un morceau de code sur
   IDLE pour vérifier. Si vous ne comprenez pas la notion, vous
   pouvez toujours nous *envoyer un mail*.
4. Si certaines notions manquaient, recommencer à l'étape 1.
   Sinon, dès que vous avez répondu à toutes les questions, allez
   comparer vos solutions avec la correction.

[[file:https://media.giphy.com/media/fQZX2aoRC1Tqw/giphy.gif]]

* Conversions
** Conversion décimal-binaire
Convertissez à la main le nombre décimal $42$ en binaire.


** Conversion binaire-décimal
Convertissez à la main le nombre binaire $1010$ en décimal.


** Conversion binaire-hexadécimal
Convertissez à la main le nombre binaire $10000100$ en hexadécimal.


** Conversion hexadécimal-décimal
Convertissez à la main le nombre hexadécimal $C0CA$ en décimal.


[[file:https://media.giphy.com/media/XCfBFvZcs5lsc/giphy.gif]]
* Variables et Types de données
** Cast de type
Demandez à l'utilisateur d'entrer un nombre entier, et affichez le résultat de ce nombre multiplié par 2.

   
** Moyenne
Demandez à l'utilisateur trois nombres décimaux /a/, /b/, /c/ et affichez la moyenne des trois.

[[file:https://media.giphy.com/media/G1ifnX4d5tYFACktp9/giphy.gif]]

* Opérateurs
** Plus ou moins
Créez deux variables /a/ et /b/ qui contiennent chacune un entier que vous choisissez.
Affichez la valeur de la somme $a+b$ et de la différence $a-b$. 

** Combien de fois
Créez une variable /a/ contenant un nombre entier que vous choisissez.
Affichez :
- La multiplication de /a/ par 3
- La division exacte de a par 3
- /a/ à la puissance 3


** Euclide
Créez une variable /a/ et donnez lui la valeur 41.
Affichez la valeur du quotient et du reste de la division euclidienne de 41 par 3.

[[file:https://media.giphy.com/media/WRQBXSCnEFJIuxktnw/giphy.gif]]
* Structures de données
** Déclaration de liste
Créez une liste /a/ contenant les nombres $1$, $3$, $9$, et le mot /salsifi/.

** Accès à une liste
Après avoir déclaré la liste /a/, affichez à l'écran le 2ème et le 4ème élément (3 et /salsifi)/

** Ajout dans une liste
Comment peut-on ajouter le nombre $42$ à fin de la liste /a/ ?

** Retirer un élément d'une liste
Comment peut-on retirer l'élément valant $3$ de la liste /a/ ?

** Déclaration de tuple
Créez un tuple /a/ contenant les nombres $42$, $35.78$, et le mot /salut/.

** Accès à un tuple
Après avoir déclaré le tuple /a/, affichez à l'écran le 2ème élément (35.78)

** Ajout dans un tuple
Peut-on ajouter la liste $1$, $2$, $3$ à fin du tuple /a/ ?

** Déclaration d'ensemble
Créez un ensemble /a/ contenant les lettres $b$, $a$, $z$ et le nombre $25$.

** Ajout dans un ensemble
Peut-on ajouter la lettre $a$ à l'ensemble /a/ ?

** Dictionnaire
Créez un dictionnaire appelé /montre/, constitué de 3 champs :
- marque
- modele
- annee

La marque sera /Tissot/, le modèle sera /Seamaster/, et l'année sera /2021/.

** Ajout d'un champ dans le dictionnaire
On voudrait ajouter un champ /mouvement/ dans le dictionnaire /montre/ qui prendra la valeur : /automatique/

** Déclaration de matrice
Créez A, une matrice 3x3, autrement dit une liste de listes.
Chaque élément de la matrice vaut zéro, excepté les éléments de la diagonale qui valent 1.

** Accès à une matrice
On souhaite changer une valeur dans la matrice A.
Après sa déclaration, changez la valeur de la case centrale, et mettez la valeur 42.

** Slicing
Déclarez une liste /alphabet/ contenant les 8 premières lettres de l'alphabet en minuscules.
Puis, utilisez du slicing pour créer une seconde liste /alpha/ contenant les lettres de /b/ à /f/.

[[file:https://media.giphy.com/media/5wWf7H89PisM6An8UAU/giphy.gif]]
* Chaines de caractères
** Concaténation
Demandez à l'utilisateur son nom, et affichez une phrase qui lui souhaite une bonne journée.
Pour cela, utilisez la *concaténation* de chaînes de caractères.

** Séparation
Demandez à l'utilisateur une phrase, et affichez le nombre de mots que vous comptez dans cette phrase.

** Slicing, encore
Créez une variable /meteo/ qui contient la phrase suivante :
/Demain il fera beau, avec quelques nuages/

Ensuite, à partir de la chaîne de caractères /meteo/, créez une
variable /prevision/ qui contient la prévision météo en extrayant
de la phrase précédente l'information importante, c'est à dire
/beau, avec quelques nuages/.
Votre méthode doit également fonctionner quand la prévision météo
change.

_Indice :_ On sait que peu importe la prévision, le début de la
phrase sera toujours "Demain il fera".

[[file:https://media.giphy.com/media/citBl9yPwnUOs/giphy.gif]]
* Conditions
** Booléens
Créez une variable /nombre/ qui contient un nombre entier (choisissez une valeur quelconque)
Affichez /True/ si le nombre est divisible par 2, sinon affichez /False/.

** Tests de divisibilité
Toujours avec une variable /nombre/ contenant un entier que vous choisissez,
- Affichez /Le nombre est divisible par deux/ si il est divisible par 2
- Affichez /Le nombre est divisible par trois/ si il est divisible par 3.
- Si il est divisible par 2 *et* 3, affichez /Le nombre est divisible par deux et par trois/.
- Si il n'est pas divisible par deux ou trois, affichez /Le nombre n'est pas divisible/.

[[file:https://media.giphy.com/media/KzyMcEfDh4Jiw/giphy.gif]]
* Boucles
** Affiche les nombres
À l'aide d'une boucle, affiche tous les nombres pairs entre 1 et 100

** Affiche N nombres
À l'aide d'une boucle, affiche les 10 premiers nombres divisibles par 27 en partant de 1.

** Liste en compréhension
Grâce à une liste en compréhension, générez une liste /cubes/ qui contient
les valeurs au cube des nombres allant de 0 à 50.

[[file:https://media.giphy.com/media/3orieKvCz1U0MlVs08/giphy.gif]]
* Fonctions
** Déclaration de fonction
Créez une fonction /cube/ qui prend en paramère un nombre /a/ et qui renvoie $a^3$

** Appel de fonction
Définissez une fonction ~somme_liste~ qui prend en paramètre une liste /L/ et qui renvoie la
somme des éléments de cette liste, calculée grâce à une boucle.

Puis, créez une liste /elements/ contenant les nombres 22, 45, 79, et 31, et affichez le résultat
de la fonction ~somme_liste~ appliquée sur cette liste.


[[file:https://media.giphy.com/media/mGK1g88HZRa2FlKGbz/giphy.gif]]
